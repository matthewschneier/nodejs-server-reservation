"""Python script to run flask directly."""
#!/bin/python3.6
from app import application
from flask_wtf.csrf import CSRFProtect
import sys

sys.path.insert(0, "/var/www/html/server_reservation")

app = application

debug = False

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8000, debug=debug)
    app.config.update(SESSION_COOKIE_SECURE=True)
