#!/var/www/html/server_reservation/learn_flask/bin/python3.6

import sys

sys.path.insert(0, "/var/www/html/server_reservation")

sys.path.insert(0, "/var/www/html/server_reservation/reservation/lib/python3.6/site-packages")

# Doesn't work if moved to beginning of document so ignore linter.
from app import application
