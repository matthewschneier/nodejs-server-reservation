function auth (req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    req.flash("error", "Unauthorized: Please login.");
    res.redirect("/login");
}

module.exports = auth;