$(function () {
    $("#calendar").fullCalendar({
        dayClick: function (date, jsEvent, view) {
            console.log("Clicked on " + date.format());
        },
        displayEventEnd: true,
        eventSources: [
            {url: "/query", color: "#ff5555"}
        ],
    });
});