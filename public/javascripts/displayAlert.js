window.onload = function () {
  const url = new URL(window.location.href);
  const alert = url.searchParams.get("alert");
  if (alert == "true") {
    const alertEl = document.getElementById("del_alert");
    alertEl.style.display = "";
    alertEl.innerText = "Your reservation has been deleted.";
  }
};
