const helmet = require("helmet");
const express = require("express");
const passport = require("passport");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const favicon = require("serve-favicon");
const flash = require("express-flash");
const bodyParser = require("body-parser");
const session = require("express-session");
const MongoStore = require("connect-mongo")(session);
const csurf = require("csurf");

const indexRouter = require("./routes/index");
const loginRouter = require("./routes/login");
const logoutRouter = require("./routes/logout");
const queryRouter = require("./routes/query");
const serverRouter = require("./routes/server");
const resRouter = require("./routes/reservation");

const app = express();

app.use(helmet());
app.use(helmet.contentSecurityPolicy({
  directives: {
    defaultSrc: ["'self'"],
    styleSrc: ["'self'", "'unsafe-inline'"],
    frameAncestors: ["'none'"],
  }
}));
app.use(helmet.hsts({
  maxAge: 60 * 60 * 24 * 365
}));

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

// set up passport
app.use(session({
  secret: process.env.SESSION_KEY,
  saveUninitialized: false,
  resave: true,
  httpOnly: true,
  store: new MongoStore({
    url: process.env.MONGO_DB_STORE,
    collection: "sessions"
  })
}));
app.use(passport.initialize());
app.use(passport.session());


app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(flash());
app.use(function (req, res, next) {
  res.locals.messages = require("express-messages");
  next();
});

app.use(logger("dev"));
app.use(cookieParser());
app.use(csurf());
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(express.static(path.join(__dirname, "public")));
app.use(favicon(path.join(__dirname, "public", "images", "favicon.ico")));

// tell if user is authenticated.
app.get("*", function (req, res, next) {
  res.locals.loggedIn = req.isAuthenticated();
  res.locals.csrfToken = req.csrfToken();
  next();
});

app.use("/", indexRouter);
app.use("/login", loginRouter);
app.use("/logout", logoutRouter);
app.use("/query", queryRouter);
app.use("/server", serverRouter);
app.use("/reservations", resRouter);

// catch 401 (unauthorized) errors
app.use(function (req, res) {
  res.status(401);
  res.render("errorPages/401");
});

// catch 404 and forward to error handler
app.use(function (req, res) {
  res.status(404);
  res.render("errorPages/404");
});

// error handler
app.use(function (err, req, res) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("errorPages/error");
});

module.exports = app;
