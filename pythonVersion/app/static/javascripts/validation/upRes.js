document.addEventListener("DOMContentLoaded", () => {
  const server = document.getElementById("server_select");
  if (server.value != "Select a server") {
    getRes();
  } else {
    server.selectedIndex = 0;
  }
});

document.getElementById("update_form").addEventListener("change", validation);
document.getElementById("server_select").addEventListener("change", getRes);
document.getElementById("res_select").addEventListener("change", update_res);
document.getElementById("button").addEventListener("click", function(e) {
  e.preventDefault();
  confirm("Are you sure you want to update your reservation? This cannot be undone.");
  document.getElementById("update_form").submit();
});

function validation() {
  const server = document.getElementById("server_select").value;
  const res = document.getElementById("res_select").value;
  const reason = document.getElementById("reason").value;
  const start = document.getElementById("start").value;
  const end = document.getElementById("end").value;
  const button = document.getElementById("button");

  if (server == "Select a server" || res == "Select a reservation" || res == ""
  || reason == "" || start == "" || end == "") {
    button.setAttribute("disabled", "");
  } else {
    button.removeAttribute("disabled");
  }
}

function getRes() {
  const user = document.getElementById("user").innerText;
  const server = document.getElementById("server_select").value;
  if (server == "Select a server") {
    return;
  } else {
    const xhttp = new XMLHttpRequest();
    xhttp.responseType = "json";
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        if (this.response.length > 0) {
          addOptions(this.response);
        }
      }
    };
    xhttp.open("GET", "/query/reservations?server=" + server + "&user=" + user, true);
    xhttp.send();
  }
}

function addOptions(reservations) {
  const res = document.getElementById("res_select");
  let list = "<option value='Select a reservation' selected>Select a reservation</option>";
  reservations.forEach(function(reservation) {
    let start = reservation.start.split(".")[0];
    let description = reservation.description;
    list +=
      "<option value='" + reservation._id + "'>" +
        description.replace(/%20/g, " ") + " + " + start +
      "</option>"
    ;
  });
  res.innerHTML = list;
}

function update_res() {
  const res = document.getElementById("res_select").value;
  if (res.length > 0 && res != "Select a reservation") {
    document.getElementById("update_res").style.display = "";
  } else {
    document.getElementById("update_res").style.display = "none";
  }
}
