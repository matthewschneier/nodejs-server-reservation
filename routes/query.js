const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

mongoose.connect(process.env.MONGO_DB, {
  user: process.env.MONGO_DB_USER,
  pass: process.env.MONGO_DB_PASS,
  useNewUrlParser: true,
  auth: {
    authdb: "admin"
  }
});
const roloSchema = Schema({
  title: String,
  user: String,
  start: Date,
  end: Date
});
const Rolo = mongoose.model("Rolo", roloSchema, "reservations");

router.get("/", (req, res, next) => {
  Rolo.find({}, "-_id title start end description", (err, reservations) => {
    if (err) {
      return next(err);
    }
    res.send(reservations);
  });
});

router.get("/reservations", (req, res, next) => {
  const server = req.query.server;
  const user = req.query.user;
  Rolo.find({
    server: server,
    user: user
  }, "_id description start", (err, reservations) => {
    if (err) {
      return next(err);
    }
    res.send(reservations);
  });
});

router.get("/:server", (req, res, next) => {
  Rolo.find({
    server: req.params.server
  }, "-_id title start end description", (err, reservations) => {
    if (err) {
      return next(err);
    }
    res.send(reservations);
  });
});

module.exports = router;
