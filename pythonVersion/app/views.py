"""Directs users to webpages and renders templates."""
from app import app
from datetime import datetime, timedelta
from email.message import EmailMessage
from smtplib import SMTP
from flask import flash, redirect
from flask import render_template, request, session
from flask_compress import Compress
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from .forms import AddForm, DelForm, PasswordForm, UpdateForm
from functools import wraps
from EmployeeLookup import EmployeeLookup as lookup
from pymongo import MongoClient
from urllib.parse import quote

# Gzip compress app.
compress = Compress(app)

# Limit requests by ip.
limiter = Limiter(
        app,
        key_func=get_remote_address,
)
RATE_LIMIT = "10 per minute"

CLIENT = MongoClient("mongodb://readWriteRes:sdfwersdrwodfgdeUIZ546Ii45fffRR\
                     kmm5HRrjjjdfffN@localhost/reservation?authSource=admin")
RES_COLLECTION = CLIENT.reservation.reservations


def is_logged_in(f):
    """Check if user has logged in."""
    @wraps(f)
    def wrap(*args, **kwargs):
        if "logged_in" in session:
            return f(*args, **kwargs)
        else:
            flash("Unauthorized. Please login.", category="danger")
            return redirect("/login")
        return wrap


def loginUser(username):
    """Login user."""
    # Automatically logout user in 3 days.
    session.permanent = True
    app.permanent_session_lifetime = 60*60*24*3
    session["logged_in"] = True
    session["username"] = username
    # Create message to flash.
    msg = "You are logged in " + username + "."
    return msg


@app.route("/")
def index():
    """Index."""
    if "logged_in" in session:
        return redirect("/server")
    return redirect("/login")


@app.route("/login/", methods=["GET", "POST"])
@limiter.limit(RATE_LIMIT)
def login():
    """Login page."""
    passform = PasswordForm()
    if passform.validate_on_submit() and request.method == "POST":
        employee = request.form["username"].replace(" ", "")
        passwd = request.form["password"]
        ActiveDirectory = lookup().ad_password_checker(employee, passwd)
        LDAP = lookup().ldap_password_checker(employee, passwd)
        if ActiveDirectory and LDAP:
            # If in right department.
            dept = lookup().ldap_lookup(employee, "departmentNumber")
            if ".EXEC" in dept or ".GTO" in dept or ".HR" in dept:
                msg = loginUser(employee)
                flash(msg, category="success")
                return redirect("/user_lookup/")
            else:
                flash("Not authorized.", category="danger")
                return redirect("/login/")
        else:
            flash("Bad username or password.", category="danger")
            return render_template(
                    "login.html",
                    title="Login",
                    form=passform
                )
    # Page before submit.
    return render_template(
            "login.html",
            title="Login",
            form=passform,
    )


@app.route("/logout/")
@is_logged_in
def logout():
    """Logout user."""
    session.clear()
    flash("You are now logged out.", category="info")
    return redirect("/")


@app.route("/query/")
def get_all_res():
    """Return all reservations."""
    return CLIENT.reservation.reservations


@app.route("/query/reservations/")
def get_all_user_res_by_server():
    """Return user reservations by server."""
    server = request.args.get("server")
    user = request.args.get("user")
    reservations = RES_COLLECTION.find({"server": server, "user": user})
    return reservations


@app.route("/query/<string:server>/")
def get_all_res_by_server(server):
    """Return all reservations on server."""
    reservations = RES_COLLECTION.find({"server": server})
    return reservations


@app.route("/server/")
@is_logged_in
def all_servers_page():
    """Display all servers page."""
    serverList = app.config["SERVER_LIST"].split(" ")
    return render_template(
        "servers.html",
        serverList=serverList
    )


@app.route("/server/<string:server>/")
def server_cal(server):
    """Display server cal."""
    return render_template(
        "serverCal.html",
        server=server
    )


@app.route("/resevations/add", methods=["GET", "POST"])
@is_logged_in
def add_reservation():
    """Add reservation."""
    addForm = AddForm()
    if addForm.validate_on_submit() and request.method == "POST":
        user = session["username"]
        server = request.form["server"]
        title = quote(request.form["title"]).replace("&#27;", "'")
        start = datetime.strptime(request.form["start"], "%Y-%m-%dT%H:%M:%S")
        end = start + timedelta(hours=int(request.form["end"]))

        res = RES_COLLECTION.find(
            {"server": server, "start": {"$lte": end}, "end": {"$gte": start}}
        )
        if len(res) > 3:
            flash(server + " is fully booked during that time.",
                  category="error")
            return redirect("/reservations/add")
        post = {
            "user": user,
            "title": user + " - " + title,
            "description": title,
            "server": server,
            "start": datetime.isoformat(start),
            "end": datetime.isoformat(end)
        }
        RES_COLLECTION.insert_one(post)
        flash("You have made your reservation", category="success")
        return redirect("/server/" + server)
    serverList = app.config["SERVER_LIST"].split(" ")
    return render_template(
        serverList=serverList,
        form=addForm
    )


@app.route("/reservations/del", methods=["GET", "POST"])
@is_logged_in
def del_res():
    """Delete reservation."""
    delForm = DelForm()
    user = session["username"]
    if delForm.validate_on_submit() and request.method == "POST":
        id = request.form["reservation"]
        server = request.form["server"]
        res = RES_COLLECTION.find({"user": user, "_id": id})
        if res:
            RES_COLLECTION.delete_one({"_id": id})
            flash("You have deleted your reservation.", category="info")
            return redirect("/server/" + server)
        else:
            flash("Unauthorized. You don't own that reservation.",
                  category="error")
            return redirect("/reservations/del")
    return render_template(
        "delReservation.html",
        user=user,
        servers=RES_COLLECTION.find({user: user}).distinct("server"),
        form=delForm
    )


@app.route("/reservations/update")
@is_logged_in
def update_res():
    """Update reservation."""
    updateForm = UpdateForm()
    user = session["username"]
    if updateForm.validate_on_submit() and request.ethod == "POST":
        id = request.form["reservation"]
        server = request.form["server"]
        description = quote(request.form["title"]).replace("&#27;", "'")
        start = datetime.strptime(request.form["start"], "%Y-%m-%dT%H:%M:%S")
        end = start + timedelta(hours=int(request.form["end"]))

        res = RES_COLLECTION.find(
            {"server": server, "start": {"$lte": end}, "end": {"$gte": start}}
        )
        if len(res) > 3:
            flash(server + " is fully booked during that time.",
                  category="error")
            return redirect("/reservations/update")

        res = RES_COLLECTION.find({"user": user, "_id": id})
        if res:
            RES_COLLECTION.update_one(
                {"_id": id},
                {"$set": {
                    "user": user,
                    "server": server,
                    "title": user + " - " + description,
                    "description": description,
                    "start": start,
                    "end": end
                }}
            )
        else:
            flash("Unauthorized. You don't own that reservation.",
                  category="error")
            return redirect("/reservations/del")

    servers = RES_COLLECTION.find({"user": user}).distinct("server")
    return render_template(
        "upReservation.html",
        servers=servers,
        user=user,
        form=updateForm
    )


# Error pages.
@app.errorhandler(500)
def error(e):
    """Python failed or username doesn't exist."""
    s = SMTP("mail.company.com:25")
    msg = EmailMessage()
    msg["From"] = "Reservation@company.com"
    msg["To"] = "mschneier@company.com"
    msg["Subject"] = "500 Error"
    msg.set_content("There was a 500 error on server-reservation. Please check it out.")
    try:
        s.send_message(msg)
    except Exception:
        pass
    return render_template(
            "error_pages/500.html",
            title="500",
            ), 500


@app.errorhandler(404)
def page_not_found(e):
    """Page not found."""
    return render_template(
            "error_pages/404.html",
            title="404",
            ), 404
