const auth = require("../middleware/auth");
const express = require("express");
const router = express.Router();
const config = require("config");

router.get("/", auth, (req, res) => {
  let serverList = config.get("serverList");
  serverList = serverList.split(" ");
  res.render("server", {
    serverList: serverList
  });
});

router.get("/:server", auth, (req, res) => {
  const server = req.params.server;
  res.render("serverCal", {
    server: server
  });
});

module.exports = router;
