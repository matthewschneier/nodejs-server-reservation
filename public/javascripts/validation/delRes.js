document.getElementById("del_form").addEventListener("change", validation);
document.getElementById("server_select").addEventListener("change", getRes);
document.getElementById("button").addEventListener("click", function(e) {
  e.preventDefault();
  confirm("Are you sure you want to delete your reservation? This cannot be undone.");
  document.getElementById("del_form").submit();
});

function validation() {
  const server = document.getElementById("server_select").value;
  const res = document.getElementById("res_select").value;
  const button = document.getElementById("button");
  if (server == "Select a server" || res == "Select a reservation" || res == "") {
    button.setAttribute("disabled", "");
  } else {
    button.removeAttribute("disabled");
  }
}

function getRes() {
  const user = document.getElementById("user").innerText;
  const server = document.getElementById("server_select").value;
  const xhttp = new XMLHttpRequest();
  xhttp.responseType = "json";
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      addOptions(this.response);
    }
  };
  xhttp.open("GET", "/query/reservations?server=" + server + "&user=" + user, true);
  xhttp.send();
}

function addOptions(reservations) {
  const tz = new Date().getTimezoneOffset();
  const res = document.getElementById("res_select");
  let list = "<option value='Select a reservation' selected>Select a reservation</option>";
  reservations.forEach(function(reservation) {
    let start = reservation.start.split(".")[0];
    start = moment(start).subtract(tz, "minutes").format("MM-DD-YYYY H:mm").replace("T"," ");
    let description = reservation.description;
    list +=
      "<option value='" + reservation._id + "'>" +
        description.replace(/%20/g, " ") + " + " + start +
      "</option>"
    ;
  });
  res.innerHTML = list;
}
