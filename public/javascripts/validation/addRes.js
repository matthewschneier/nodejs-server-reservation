["blur", "change", "click", "input", "keyup"].forEach(function(e) {
  document.getElementById("add_form").addEventListener(e, validation, false);
});

function validation() {
  const server = document.getElementById("server_select").value;
  const reason = document.getElementById("reason").value;
  const start = document.getElementById("start").value;
  const end = document.getElementById("end").value;
  const button = document.getElementById("button");

  if (server == "" || server == "Select a server" || reason == "" ||
      start == "" || end == "") {
    button.setAttribute("disabled", "");
  } else {
    button.removeAttribute("disabled");
  }
}
