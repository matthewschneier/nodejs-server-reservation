"""Create forms for reservations."""
from flask_wtf import FlaskForm
from wtforms import DateTimeField, IntegerField, PasswordField
from wtforms import StringField, validators


class AddForm(FlaskForm):
    """Add reservation."""

    server = StringField("server", [validators.Length(min=2, max=999),
                                    validators.DataRequired()])
    title = StringField("title", [validators.Length(min=2, max=999),
                                  validators.DataRequired()])
    start = DateTimeField("start", [validators.DataRequired()])
    end = IntegerField("end", [validators.DataRequired(),
                               validators.NumberRange(min=1, max=72)])


class DelForm(FlaskForm):
    """Delete reservation."""

    reservation = StringField("reservation",
                              [validators.Length(min=2, max=999),
                               validators.DataRequired()])
    server = StringField("server", [validators.Length(min=2, max=999),
                                    validators.DataRequired()])


class PasswordForm(FlaskForm):
    """Login form."""

    username = StringField("username", [validators.Length(min=2, max=50),
                                        validators.DataRequired()])
    password = PasswordField("password", [validators.Length(min=2, max=999),
                                          validators.DataRequired()])


class UpdateForm(FlaskForm):
    """Update reservation."""

    id = StringField("id", [validators.Length(min=2, max=50),
                            validators.DataRequired()])
    server = StringField("server", [validators.Length(min=2, max=999),
                                    validators.DataRequired()])
    title = StringField("title", [validators.Length(min=2, max=999),
                                  validators.DataRequired()])
    start = DateTimeField("start", [validators.DataRequired()])
    end = IntegerField("end", [validators.DataRequired(),
                               validators.NumberRange(min=1, max=72)])
