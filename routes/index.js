const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
  if (req.isAuthenticated()) {
    res.redirect("server");
  } else {
    res.redirect("/login");
  }
});

module.exports = router;
