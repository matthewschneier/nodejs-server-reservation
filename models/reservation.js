const mongoose = require("mongoose");
const BaseJoi = require("joi");
const Extension = require("joi-date-extensions");
const Joi = BaseJoi.extend(Extension);
const moment = require("moment-timezone");

const earliest = moment().subtract(1, "days").format();
const latest = moment().add(3, "years").add(3, "days").format();

const reserveSchema = new mongoose.Schema({
  server: {
    type: String,
    maxlength: 255,
    required: true,
  },
  title: {
    type: String,
    required: true,
    minlength: 2,
    maxlength: 255,
  },
  description: {
    type: String,
    required: true,
  },
  user: {
    type: String,
    minlength: 2,
    maxlength: 99,
    required: true,
  },
  start: {
    type: Date,
    min: earliest,
    max: latest,
    required: true,
  },
  end: {
    type: Date,
    min: earliest,
    max: latest,
    required: true,
  },
});
const Reservation = mongoose.model("Reservation", reserveSchema);

function validateReservation(reservation) {
  const schema = {
    title: Joi.string().min(3).max(255).required(),
    server: Joi.string().required(),
    start: Joi.date().min(earliest).max(latest).required(),
    end: Joi.number().min(1).max(72).required(),
  };
  return Joi.validate(reservation, schema);
}

exports.reserveSchema = reserveSchema;
exports.Reservation = Reservation;
exports.validate = validateReservation;
