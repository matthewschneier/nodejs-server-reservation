function jsUcfirst(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

$(function () {
  const $serverName = jsUcfirst($("#serverName").text().split(" ")[0]);
  let server = document.getElementById("serverName");
  server.innerText = jsUcfirst(server.innerText);
  server = server.innerText;
  $("#calendar").fullCalendar({
    displayEventEnd: true,
    eventSources: [{
      url: "/query/" + $serverName,
      color: "#ff5555",
    }],
    eventRender: function (event, element) {
      element.qtip({
        content: event.description
      });
    },
    timezone: "local",
  });
});
