const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
   req.logout();
   req.flash("info", "You are now logged out.");
   res.redirect("/login");
});

module.exports = router;
