window.onload = function() {
  const username = document.getElementById("username").value;
  const password = document.getElementById("password").value;
  const usernameLength = username.length;
  const passwordLength = password.length;
  const button = document.getElementById("submitButton");

  //Check if username and password were automatically filled in.
  if ((usernameLength > 2) && (usernameLength < 30) &&
      (username.match(/^[a-zA-Z]+$/)) && (passwordLength > 1)) {
    button.removeAttribute("disabled");
  }
};

["blur", "change", "click", "input", "keyup"].forEach(function(e) {
  document.getElementById("login_form").addEventListener(e, validation, false);
});

function validation() {
  const username = document.getElementById("username").value;
  const password = document.getElementsByName("password").value;
  const button = document.getElementById("submitButton");
  if (username == "" || password == "") {
    button.setAttribute("disabled", "");
  } else {
    button.removeAttribute("disabled");
  }
}
