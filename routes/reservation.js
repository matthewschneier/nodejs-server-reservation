const {
  Reservation,
  validate
} = require("../models/reservation");
const express = require("express");
const router = express.Router();
const auth = require("../middleware/auth");
const moment = require("moment-timezone");
const config = require("config");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const validator = require("validator");

mongoose.connect(process.env.MONGO_DB, {
  user: process.env.MONGO_DB_USER,
  pass: process.env.MONGO_DB_PASS,
  useNewUrlParser: true,
  auth: {
    authdb: "admin"
  }
});
const changeSchema = Schema({
  title: String,
  user: String,
  start: Date,
  end: Date
});
const Change = mongoose.model("Change", changeSchema, "reservations");

router.get("/add", auth, (req, res) => {
  let serverList = config.get("serverList");
  serverList = serverList.split(" ");
  res.render("addReservation", {
    serverList: serverList
  });
});

router.post("/add", auth, (req, res, next) => {
  let server = req.body.server,
    title = validator.escape(req.body.title).replace("&#x27;", "'"),
    start = new Date(req.body.start),
    end = req.body.end;

  // Joi input validation.
  const {
    error
  } = validate({
    title,
    server,
    start,
    end
  });
  if (error) {
    req.flash("error", error.details[0].message);
    return res.redirect("/reservations/add");
  }

  // Prevent more than 4 concurrent reservations on the server.
  end = new Date(moment(start).add(end, "hours").format());
  Reservation.find({
    server: server,
    start: {
      $lte: end
    },
    end: {
      $gte: start
    }
  }, (err, docs) => {
    if (err) {
      return next(err);
    }
    if (docs.length > 3) {
      req.flash("error", server + " is fully booked during this time.");
      return res.redirect("/reservations/add");
    } else {
      const reservation = new Reservation({
        user: req.user.uid,
        title: req.user.uid + " - " + title,
        description: title,
        server: server,
        start: start,
        end: end
      });
      reservation.save();
      res.redirect("/server/" + server);
    }
  });
});

router.get("/del", auth, (req, res, next) => {
  const user = req.user.uid;
  Change.find({
    user: user
  }).distinct("server", (err, servers) => {
    if (err) {
      return next(err);
    }
    res.render("delReservation", {
      user: user,
      servers: servers
    });
  });
});

router.post("/del", auth, (req, res) => {
  const user = req.user.uid;
  const id = req.body.reservation;
  const server = req.body.server;

  // Validate user owns reservation before deleting.
  Change.find({
    user: user,
    _id: id
  }, (err, reservation) => {
    if (err) {
      return res.send(err);
    }
    if (reservation) {
      Change.findOneAndDelete({
        _id: id
      }, (err, doc) => {
        if (err) {
          return res.status(500).send(err);
        } else {
          req.flash("info", "You have deleted your reservation.");
          res.redirect("/server/" + server);
        }
      });
    } else {
      res.status(401).send("Unauthorized.");
    }
  });
});

router.get("/update", auth, (req, res, next) => {
  const user = req.user.uid;
  Change.find({
    user: user
  }).distinct("server", (err, servers) => {
    if (err) {
      return next(err);
    }
    res.render("upReservation", {
      user: user,
      servers: servers
    });
  });
});

router.post("/update", auth, (req, res, next) => {
  const user = req.user.uid;
  const id = req.body.reservation;
  const server = req.body.server;
  let description = validator.escape(req.body.title).replace("&#x27;", "'"),
    start = new Date(req.body.start),
    end = req.body.end,
    title = user + " - " + description;

  // Joi input validation.
  const {
    error
  } = validate({
    title,
    server,
    start,
    end
  });
  if (error) {
    req.flash("error", error.details[0].message);
    return res.redirect("/reservations/update");
  }

  // Prevent more than 4 concurrent reservations on the server.
  end = new Date(moment(start).add(end, "hours").format());
  Reservation.find({
    server: server,
    start: {
      $lte: end
    },
    end: {
      $gte: start
    }
  }, (err, docs) => {
    if (err) {
      return next(err);
    }
    if (docs.length > 3) {
      req.flash("error", server + " is fully booked during this time.");
      return res.redirect("/reservations/add");
    } else {
      // Validate user owns reservation before updating.
      Reservation.find({
        user: user,
        _id: id
      }, (err, reservation) => {
        if (err) {
          return res.send(err);
        }
        if (reservation) {
          Reservation.findOneAndUpdate({
            _id: id
          }, {
            $set: {
              user: user,
              title: title,
              description: description,
              start: start,
              end: end
            }
          }, {
            new: true
          }, (err, doc) => {
            if (err) {
              return res.status(500).send(err);
            } else {
              req.flash("info", "You have changed your reservation.");
              res.redirect("/server/" + server);
            }
          });
        } else {
          res.status(401).send("Unauthorized.");
        }
      });
    }
  });
});

module.exports = router;
