const express = require("express");
const router = express.Router();
const ldap = require("passport-ldapauth");
const passport = require("passport");
const fs = require("fs");

const OPTS = {
  server : {
    url: process.env.LDAP_SERVER,
    searchBase: process.env.LDAP_BASE,
    searchFilter: "(uid={{username}})",
    tlsOptions: {
      ca: [
        fs.readFileSync("/etc/pki/tls/certs/certificate.crt")
      ]
    }
  }
};
passport.use(new ldap(OPTS));
passport.serializeUser(function(user, done) {
  done(null, user);
});
passport.deserializeUser(function(user, done) {
  done(null, user);
});

router.get("/", (req, res) => {
  res.render("login");
});

router.post("/", passport.authenticate("ldapauth", {failureRedirect: "/login", failureFlash: true}),
  (req, res) => {
    const dept = req.user.departmentNumber;
    if (dept.includes("IT") || dept.includes("EXEC")) {
      req.flash("success", "You are now logged in.");
      res.redirect("/server");
    } else {
      req.logout();
      req.flash("error", "Unauthorized");
      res.redirect("/login");
    }
  }
);

module.exports = router;
